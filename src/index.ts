import log from "@ajar/marker";
import { saySomething } from "./myModule.js";
import fs from 'fs/promises';
//import Utilles from './Utilles';

const response = saySomething("hello");
log.magenta(response);

const generateID = () => Math.random().toString(32).substring(2);

interface Task {
    id: string;
    description: string;
    completed: boolean;
}

//const utilles = new Utilles();


const myObject = {
    "create": (todosList: Task[], itemsToCreateArray: string[]): Task[] => {
        for(const task of itemsToCreateArray) {
            todosList.push({id: generateID(), description: task, completed: false});
        }

        return todosList;
    },
    "read": (todosList: Task[]) =>{
        for(const task of todosList) {
            log.magenta(task.description);
        }
    },
    "update": (todosList: Task[], idsArrayToUpdate: string[]): Task[] => {
        for(let i = 0; i < todosList.length; i++) {
            if(idsArrayToUpdate.includes(todosList[i].id)) {
                todosList[i].completed = !todosList[i].completed;
            }
        }

        return todosList;
    },
    "delete": (todosList: Task[], idsToDelete: string[]): Task[] => {
        const newTodosList : Task[] = [];

        for(let i = 0; i < todosList.length; i++) {
            if(!idsToDelete.includes(todosList[i].id)) {
                newTodosList.push(todosList[i]);
            } 
        }

        return newTodosList;
    },
    "display": (todosList: Task[]) => {
        for(const task of todosList) {
            log.magenta(`Id: ${task.id} ${task.description}: ${task.completed ? 'uncomplete' : 'completed'}`);
        }
    },
    "removeCompleted": (todosList: Task[]) => {
        const newTodosList : Task[] = [];

        for(let i = 0; i < todosList.length; i++) {
            if(todosList[i].completed === false) {
                newTodosList.push(todosList[i]);
            } 
        }

        return newTodosList;
    },
    "help": () => {
        log.magenta("Optional commands: create, read, update, delete, display, remove completed, help" );
    }
}

const save = async (path: string, data: Task[]) => {
    await fs.writeFile(path, JSON.stringify(data, null, 2));
}

const init = async () => {
    const mainDir = "./";

    let fileExist = false;
    const dir = await fs.readdir(mainDir);

    for(const file of dir) {
        if(file === "tasks.json") {
            fileExist = true;
        } 
    }
    if(fileExist === false) {
        save(`${mainDir}tasks/json`, []);
    }
    
    const path = "./tasks.json";
    let data = await fs.readFile(path, "utf-8");
    let todosList : Task[] = JSON.parse(data);
    

    const myArgv = process.argv.slice(2);

    switch(myArgv[0]) {
        case 'create':
            todosList = myObject.create(todosList, myArgv.slice(1));
            save(path, todosList);
            break;
        case 'read':
            myObject.read(todosList);
            break;
        case 'update':
            todosList = myObject.update(todosList, myArgv.slice(1));
            save(path, todosList);
            break;
        case 'delete':
            todosList = myObject.delete(todosList, myArgv.slice(1));
            save(path, todosList);
            break;
        case 'display':
            myObject.display(todosList);
            break;
        case 'remove completed':
            todosList = myObject.removeCompleted(todosList);
            save(path, todosList);
            break;
        case 'help':
            myObject.help();
        default:
            myObject.help();
            break;
    }

    save(path, todosList);

    //log.green("Done");
}

init();





